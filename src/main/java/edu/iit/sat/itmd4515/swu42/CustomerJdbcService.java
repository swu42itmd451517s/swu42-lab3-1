/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.swu42;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author sujia wu
 */
@Stateless
public class CustomerJdbcService {
    
    private static final Logger LOG = Logger.getLogger(CustomerJdbcService.class.getName());
    
    @Resource(lookup = "jdbc/itmd4515DS")
    private DataSource dataSource;
    
    public Customer findCustomer(Long id){
     Customer c = null;
  
        try(Connection con = dataSource.getConnection()){  
            PreparedStatement ps = con.prepareStatement("select * from customer where customerId = ?");
            ps.setInt(1, id.intValue());
            
            ResultSet rs = ps. executeQuery();
            if(rs.next()){
             c = new Customer();
             c.setCustomerId(id);
             c.setFirstName(rs.getString("FirstName"));
             c.setLastName(rs.getString("LastName"));
             c.setEmail(rs.getString("Email"));
             LOG.info(c.toString());
        }
            
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
      
            
        return c;   
    }      
}
