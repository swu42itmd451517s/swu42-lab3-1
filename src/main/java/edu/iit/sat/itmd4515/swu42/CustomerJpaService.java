/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.swu42;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author constance
 */
@Stateless
public class CustomerJpaService {
    
    @PersistenceContext(unitName = "itmd4515PU")
    private EntityManager em;
    
    public CustomerJpaService(){
        
    }
    
    public List<Customer> findAllCustomers(){
       return em.createQuery("select c from Customer c", Customer.class).getResultList();
    }
}
